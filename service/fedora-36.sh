#!/bin/bash

echo "BUILDING FEDORA 36 PACKAGE FOR: $APP_BINARY"
echo "========================================"

name=`$APP_BINARY name`
description=`$APP_BINARY description`
version=`$APP_BINARY version`

echo "name: $name"
echo "description: $description"
echo "version: $version"

source="./deploy/service/$APP_NAME-fedora-36"
destination="/usr/local/sbin/$APP_NAME"

echo "$source -> $destination"

fpm -s dir -t rpm \
  -p "$APP_NAME-$version-fedora-36.rpm" \
  --verbose \
  --log info \
  -n $name \
  -v $version \
  -m "szabobogdan3@gmail.com" \
  --description "$description" \
  --after-install=./deploy/service/script.sh \
  -d "openssl" \
  ./deploy/service/etc/=/etc \
  ./deploy/service/usr/=/usr \
  ./deploy/service/srv/=/srv \
  $source=$destination
