#!/bin/bash

systemctl daemon-reload || true

# user setup
groupadd gisc || true
adduser --system --no-create-home -g gisc gisc || true
usermod -a -G gisc gisc || true

# executable rights
chown gisc:gisc /usr/local/sbin/{{app_name}}
chcon -R -t bin_t /usr/local/sbin/{{app_name}} || true

# settings rights
chown -R gisc:gisc /etc/gis-collective
chown -R gisc:gisc /srv/gis-collective

# restart the service if it is running
if systemctl is-active {{app_name}} ; then
    systemctl restart {{app_name}};
fi
