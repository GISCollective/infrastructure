#!/bin/bash

echo "BUILDING UBUNTU 20 PACKAGE FOR: $APP_BINARY"
echo "========================================"

name=`$APP_BINARY name`
description=`$APP_BINARY description`
version=`$APP_BINARY version`

echo "name: $name"
echo "description: $description"
echo "version: $version"

source="./deploy/service/$APP_NAME-ubuntu-21"
destination="/usr/local/sbin/$APP_NAME"

echo "$source -> $destination"

fpm -s dir -t deb \
  -p "$APP_NAME-$version-ubuntu-21.deb" \
  --verbose \
  --log info \
  -n $name \
  -v $version \
  -m "szabobogdan3@gmail.com" \
  --description "$description" \
  --after-install=./deploy/service/script.sh \
  -d "libssl1.1" \
  $source=$destination \
  ./deploy/service/etc/=/etc \
  ./deploy/service/srv/=/srv \
  ./deploy/service/usr/=/usr
