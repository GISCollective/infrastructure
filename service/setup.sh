#!/bin/bash

echo "Prepare app: $APP_BINARY"
echo "========================================"

name=`$APP_BINARY name`
description=`$APP_BINARY description`
version=`$APP_BINARY version`

mkdir -p deploy/service/usr/lib/systemd/system/

cp submodules/infrastructure/service/fedora-39.sh deploy/service
cp submodules/infrastructure/service/ubuntu-22.sh deploy/service
cp submodules/infrastructure/service/script.sh deploy/service
cp submodules/infrastructure/service/template.service deploy/service/usr/lib/systemd/system/$name.service

echo "All templates copied"

sed -i "s/{{app_name}}/$name/g" deploy/service/script.sh

sed -i "s/{{app_name}}/$name/g" deploy/service/usr/lib/systemd/system/$name.service
sed -i "s/{{description}}/$description/g" deploy/service/usr/lib/systemd/system/$name.service

echo "All variables replaced"
